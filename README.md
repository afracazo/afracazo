# Hello, 👋 

Hi, all. I'm a product designer based in Australia 🇦🇺 (UTC +10). You can find me across the web under `fracazo`, usually.


# Interests

I am always eager to expand my knowledge and try out new activities and interests that capture my interest. If you share any of these passions, I would love to chat with you over a cup of coffee. Let me know which ones intrigue you.

Here are some of the topics I'm into:

* Prototypes using a diverse tools, like many designers I'm into figma now
* Visual design
* User research
* Lean user experience processes
* Front end development
* Ocean swimming
* Scuba diving
* Camping
* Mountain biking
* 4WD & exploring nature
* Hiking
* Bushcraft
* Cooking (I can bake a delicious organic granola)
* Dancing forró
* Learning languages

# Working

I work as a Senior Product Designer at GitLab in the group **Plan:knowledge**.

- I generally prefer written communication as I like time to digest information. However, I think sync meetings are better when collaborating on something that's ambiguous or when giving or receiving critical feedback
- I prefer meetings in the morning, but I occassionally do meetings in my evening to speak with people in EMEA

Please, if I'm taking too long getting back to you, feel free to ping me directly via slack, email or any other way you see fit. Apologies and thank you.

# Elsewhere

* https://www.linkedin.com/in/fracazo/
* https://www.instagram.com/fracazo/
* https://www.alexfracazo.com/